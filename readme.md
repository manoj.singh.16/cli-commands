## `cd`

to change working directory.

## `ls`

to list directory content.

```bash
'-a' - list all files
'-A' - almost all except . and ..
'-r' - reverse
'-R' - recursive
'-l' - long form
'-h' - human readable form
'-t' - sort by time, newest first
'-S' - sort by file size, largest first
```

## `tree`

to view directory tree

## `mkdir <dir name>`

to create directory

`-p` - It will create the parent dir if it do not exist

## `rm`

to remove files and dir.

```bash
'-r' - recursive
'-f' - force
```

## `rmdir`

to remove empty dir.

## `touch`

to create empty files.

## `echo`

to display a line of content.

## `cat`

to output concatinated content.

## `more`

to display large text with pagination but it lacks backward scroll.

## `less`

to display large text with pagination additional features than more.

## `head`

to output the first part of files

```bash
'-n' - no of lines to output
```

## `tail`

to output the last part of files

```bash
'-n' - no of lines to output
```

## `tee`

to write to standard output or file

```bash
'-a' - append to file.
```

## `curl`

to transfer a url

```bash
'-O' - to output to a file with name same as remote file.\
'-o' - to output to a file with name.
```

## `sort <filename>`

to sort the content of a file.

```bash
'-r' - reverse sorting
'-f' - case in sensitive sorting
'-n' - numerical sorting
```

## `wc`

to print newline, word, and byte counts for each file

```bash
'-m': count chars
'-w': count words
'-l': count lines
```

## `uniq`

to omit repeated lines.

```bash
'-u': only print repeated lines
'-c': prefix lines by no of occurances
'-i': ignore cases
'-D' : print all duplicates
```

## `grep`

to print lines that matches pattern.

```bash
'-w' - match only whole word
'-i' - case insensitive
'-B <num>' - no of lines before match
'-A <num>' - no of lines after match
'-C <num>' - no of lines before and after match
'-n' - print the line before match.
'./*' - to search everyting in the current dir
'-r' - recursive search
'-l' - prints the file name that has a match
'-c' - print the no of matches
'-P' - pro-campatible regex expressions
'-v' - shows the lines that donot match
```

# **Permissions and Ownership**

## `chmod`

to change files permissions

```bash
'-R' - recursively
'-v' - verbose
'-f' - forces

mode:
no permissions - 0
r - 4
w - 2
x - 1

references:
u: owner of the file
g: users who are member of file\'s group
o: others
a: all (ugo)

operators:
+: add mode
-: remove mode
=: exact mode

Example:

chmod -R 755 <filename>
or
chmod u+r filename
```

## `chown`

to change file owner and group.

## `chgrp`

to change group ownership.

# **Process Management**

## `ps`

to list a snapshot of processes.

```bash
'a'  - all process that runs on terminal.
'-e' - all running deamons.
'-l' - more information on processes.
'x'  - all process that do not run on terminal.
'-f' - with full options.
'u'  - user oriented format.
'aux' - all processes running on or off the terminal and format the result with additional information.
```

## `df`

to get the snapshot of current disk space usage.

```bash
'-h' - human readable
```

## `free`

to display amount of free and used memory in the system.

```bash
'-h' - human readable
```

## `nice -n <value> <command>`

to start a process with a nice value.

#### Note: Priority index of a process is called nice.

```
Lowest priority value: -20 and Highest priority value: 20
```

## `sudo renice -<value> -p <PID>`

to change priority of a process.

# **Networking Commands**

## `hostname`

to show the Host/Domain name and ip address of the system.

```bash
'-I'- show ip address of the system
'-d'- displays the domain name the machine belongs to
'-f'- displays the fully qualified hostname and domain name

Example:

hostname
or
hostname -I
```

## `ping`

to check if network connection is established or speed of the network

```bash
'-c <num>' - no of packets to send.

Example: ping -c 10 www.google.com
```

## `nslookup`

to discover ip and hostname.

```bash
Example:

nslookup google.com
```

## `traceroute`

to print the route packets trace to network host.

```bash
Example:

traceroute google.com
```

## `ifconfig`

to display network interface configuration

```bash
'-a' - to display all network interface configuration
'up' - to activate the interface.
'down' - to shut down the interface.

Example:

ifconfig
or
ifconfig -a
or
ifconfig <interface name>
```

## `netstat`

to Print network connections, routing tables, interface statistics, masquerade connections, and multicast memberships.

```bash
'-t' - to list all tcp ports
'-u' - to list all udp ports
'-a' - to list all ports
'-l' - to list all listening ports
'-p' - to show the PID and name of the program
```

# **Redirections**

```bash
0  : stdin

1  : stdout

2  : stderr

>  : stdout

2> : stderr

<  : stdin
```

```bash
command > filename 2>&1 or command &> filename

# to write both stdout and stdin to a file.
```

# **Pipes ( | )**

# **Different Ways to write into a file**

```bash
cat > filename or cat >> filename
```

```bash
echo "This is content through piping" | tee filename
```

```bash
nano filename
```

```bash
echo "This is content through redirection" > filename
or
echo "This is content through redirection" >> filename
```

```bash
vim filename
```
